import logo from "./logo.svg";
import "./App.css";
import Glass from "./Glass/Glass";
import Ex_Shoes from "./Ex_ShoesShop/Ex_Shoes";

function App() {
  return (
    <div className="App">
      {/* <Glass /> */}
      <Ex_Shoes />
    </div>
  );
}

export default App;
