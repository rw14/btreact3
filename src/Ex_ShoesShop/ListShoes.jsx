import React, { Component } from "react";
import ItemShoes from "./ItemShoes";

export default class ListShoes extends Component {
  render() {
    return (
      <div className="row">
        {this.props.data.map((item, index) => {
          return (
            <div className="col-3">
              <ItemShoes
                handleView={this.props.handleViewDetail}
                detail={item}
                handleCart={this.props.handleCart}
              />
            </div>
          );
        })}
      </div>
    );
  }
}
