import React, { Component } from "react";

export default class extends Component {
  render() {
    let { name, image, price, description, shortDescription } =
      this.props.detailShoes;
    return (
      <div className="container p-5">
        <div className="row">
          <div className="col-4 mt-3 ">
            <img src={image} alt="" />
          </div>
          <div className="col-8">
            <h5 className="font-italic">Name : {name} </h5>
            <h2 className="bg-primary">Price :{price} $ </h2>
            <p className="py-1">description :{description} </p>
          </div>
        </div>
      </div>
    );
  }
}
