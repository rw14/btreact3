import React, { Component } from "react";

export default class ItemShoes extends Component {
  render() {
    let { name, image, price, description, shortDescription } =
      this.props.detail;
    return (
      <div className="card my-2" style={{ width: "18rem" }}>
        <img className="card-img-top" src={image} alt="Card image cap" />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          {/* <h5 className="card-title2 text-primary"></h5> */}
          {/* <p className="card-text"></p> */}
          {/* <a href="#" className="btn btn-primary"></a> */}
          <button
            onClick={() => {
              this.props.handleView(this.props.detail.id);
            }}
            className="btn btn-success ml-2"
          >
            View Detail
          </button>
          <br />
          <button
            onClick={() => {
              this.props.handleCart(this.props.detail);
            }}
            className=" mt-2 btn btn-dark text-white"
          >
            Add to Cart
          </button>
        </div>
      </div>
    );
  }
}
