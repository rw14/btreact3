import React, { Component } from "react";

export default class Cart extends Component {
  rederTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td className="font-weight-bold">{item.name}</td>
          <td>
            {item.price}{" "}
            <span className="text-danger display-5 font-weight-bold">$</span>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDecrease(item);
              }}
              className="btn btn-warning"
              id="-1"
            >
              -
            </button>
            <span className="mx-3">{item.soluong}</span>
            <button
              onClick={() => {
                this.props.handleIncrease(item);
              }}
              className="btn btn-primary"
              id="1"
            >
              +
            </button>
          </td>
          <td>
            {""}
            <img src={item.image} style={{ width: 70 }} alt=""></img>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleRemove(item.id);
              }}
              className="btn btn-danger"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="container py-5">
        <table className="table text-left">
          <thead>
            <tr className="font-weight-bold">
              <td>Name </td>
              <td>Price</td>
              <td>Quantiny</td>
              <td>Image</td>
            </tr>
          </thead>
          <tbody>{this.rederTbody()}</tbody>
        </table>
        {this.props.cart.length == 0 && <p>Empty </p>}
      </div>
    );
  }
}
