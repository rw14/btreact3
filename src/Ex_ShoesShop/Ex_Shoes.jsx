import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoes } from "./data_shoes";
import DetailShoes from "./DetailShoes";
import ListShoes from "./ListShoes";

export default class Ex_Shoes extends Component {
  state = {
    shoesArr: dataShoes,
    detailShoes: dataShoes[0],
    cart: [],
  };
  handleViewDetail = (idShoes) => {
    let index = this.state.shoesArr.findIndex((item) => {
      return item.id == idShoes;
    });
    // if (index !== -1) {
    // }
    index !== -1 &&
      this.setState({
        detailShoes: this.state.shoesArr[index],
      });
  };
  // /cart
  handleCart = (shoes) => {
    // khong nen push truc tiep vao Cart ==>clone ra r gan vao cart trong state
    let cloneCart = [...this.state.cart];
    //
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoes.id;
    });
    // cloneCart.push(shoes);
    // 1 sp chua co trong gio hang
    if (index == -1) {
      let productCart = { ...shoes, soluong: 1 };
      // console.log("sp gio hang", productCart);
      cloneCart.push(productCart);
    } else {
      // 2 sp co trong gio hang
      // let sp = this.state.cart[index];
      // sp.soluong++;
      cloneCart[index].soluong++;
    }

    this.setState(
      {
        cart: cloneCart,
      }
      // () => {
      //   // tu dong chay sau khi setstate
      //   console.log(this.state);
      // }
    );
    // console.log(this.state.cart.length);
    // khong chay dc clg vi state chay sau cung`
  };
  handleRemove = (idShoes) => {
    let cloneCart = [...this.state.cart];
    console.log("id", idShoes);
    let index = this.state.cart.findIndex((item) => {
      return item.id == idShoes;
    });
    if (index !== -1) {
      cloneCart.splice(index, 1);
      console.log("list cart", cloneCart);
    }
    this.setState({
      cart: cloneCart,
    });
  };
  handleIncrease = (shoes) => {
    let cloneCart = [...this.state.cart];
    //
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoes.id;
    });
    cloneCart[index].soluong++;
    this.setState({
      cart: cloneCart,
    });

    // console.log(cloneCart.soluong);
  };
  handleDecrease = (shoes) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoes.id;
    });
    cloneCart[index].soluong--;
    this.setState({
      cart: cloneCart,
    });
  };
  render() {
    // console.log(this.state.cart.length);
    return (
      <div>
        <Cart
          cart={this.state.cart}
          handleRemove={this.handleRemove}
          handleDecrease={this.handleDecrease}
          handleIncrease={this.handleIncrease}
        />
        <ListShoes
          data={this.state.shoesArr}
          handleViewDetail={this.handleViewDetail}
          handleCart={this.handleCart}
        />
        <DetailShoes detailShoes={this.state.detailShoes} />
      </div>
    );
  }
}
